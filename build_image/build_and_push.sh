#!/bin/bash
set -e

sudo docker build -t registry-itsf.local.domain:5000/nginx-custom:unprivileged .
sudo docker push registry-itsf.local.domain:5000/nginx-custom:unprivileged
