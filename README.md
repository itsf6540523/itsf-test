# ITSF TEST
## Requirements
- K3s cluster with 2 nodes(1 CP/WRK, 1WRK, Ubuntu 24.04), libvirt is used here but others hypervisors or virtualbox should also works.
- Name of the nodes: k3s-node1, k3s-node2

## Generate tls certs and secrets
```
# Inspired from https://pki-tutorial.readthedocs.io/en/latest/simple/
cd gencerts
./pki.sh hello-itsf.local.domain
./pki.sh hello-risf.local.domain
./pki.sh registry-itsf.local.domain
```
## Deploy registry
Create registry path on each node in order to create PV.  
```
mkdir -p /registry/data
```
Setup insecure registry on each node:  
/etc/rancher/k3s/registries.yaml  
```
configs:
  "registry-itsf.local.domain:5000":
    tls:
      insecure_skip_verify: true
```
:warning: You need to be able to resolve the dns registry from node to pull images. A simple way is to add an entry in your /etc/hosts on each node.
Deploy  
```
kubectl apply -f registry.yaml
```

## Create path and data for each node in order to create PVs  
```
mkdir -p /data/web
```
/data/web/index.html  
```
<!DOCTYPE html>
<center>HELLO ITSF</center>
```
## Build and Push docker image
:warning: You need to be able to resolve the dns registry to push images. A simple way is to add an entry in your /etc/hosts on your workstation.
```
cd build_image 
./build_and_push.sh
```
## Kustomize
### Deploy RISF
```
kubectl apply -k overlays/risf/
```
### Deploy ITSF
```
kubectl apply -k overlays/itsf/
```
