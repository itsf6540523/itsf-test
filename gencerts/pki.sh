#!/bin/bash
set -e
if [ "$1" == "-h" ]
  then echo "Usage: $0 fqdn [KUBENAMESPACE]"
  exit 1
fi

FQDN=$1
shift

[ -z "${FQDN}" ] && read -p 'fqdn ? ' FQDN

[ ! -d crl ] && mkdir crl
[ ! -d certs ] && mkdir certs

# Prepare ca root
if [ ! -e ca/root-ca/private/root-ca.key ] && [ ! -e ca/root-ca.crt ]
  then
    mkdir -p ca/root-ca/private ca/root-ca/db
    chmod 700 ca/root-ca/private
    cp /dev/null ca/root-ca/db/root-ca.db
    echo 01 > ca/root-ca/db/root-ca.crt.srl
    echo 01 > ca/root-ca/db/root-ca.crl.srl
    openssl req -new \
        -config root-ca.conf \
        -out ca/root-ca.csr \
        -keyout ca/root-ca/private/root-ca.key
    
    openssl ca -notext -selfsign \
        -config root-ca.conf -batch \
        -in ca/root-ca.csr \
        -out ca/root-ca.crt \
        -extensions root_ca_ext
fi

# Prepare signing CA 
if [ ! -e ca/signing-ca/private/signing-ca.key ] && [ ! -e ca/signing-ca.crt ]
  then
    mkdir -p ca/signing-ca/private ca/signing-ca/db crl certs
    chmod 700 ca/signing-ca/private    
    cp /dev/null ca/signing-ca/db/signing-ca.db
    echo 01 > ca/signing-ca/db/signing-ca.crt.srl
    echo 01 > ca/signing-ca/db/signing-ca.crl.srl
    openssl req -newkey rsa:2048 -config signing-ca.conf -keyout ca/signing-ca/private/signing-ca.key -nodes \
       -out ca/signing-ca.csr
    openssl ca -notext \
      -config root-ca.conf -batch \
      -in ca/signing-ca.csr \
      -out ca/signing-ca.crt \
      -extensions signing_ca_ext
fi

# Generate server cert

SAN=DNS:${FQDN} openssl req -newkey rsa:2048 -config server.conf -keyout certs/${FQDN}.key -nodes \
    -subj "/DC=domain/DC=local/C=FR/ST=PACA/L=Nice/O=itsf/CN=${FQDN}" \
    -out certs/${FQDN}.csr

openssl ca -notext \
    -config signing-ca.conf -batch \
    -in certs/${FQDN}.csr \
    -out certs/${FQDN}.crt \
    -extensions server_ext

# Create Fullchain certificates
cat certs/${FQDN}.crt ca/signing-ca.crt ca/root-ca.crt > certs/${FQDN}-FULL.crt

# Create kube secret

kubectl create secret tls ${FQDN} \
     --cert=certs/${FQDN}-FULL.crt \
     --key=certs/${FQDN}.key -n ${1:-default}
