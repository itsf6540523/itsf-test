#!/bin/bash
set -e

if [[ "$1" == "-h" || -z "$1" ]]
  then echo "Usage: $0 ca/signing-ca/example-serial.pem"
  exit 1
fi

openssl ca \
    -config signing-ca.conf \
    -revoke ca/signing-ca/${1} \
    -crl_reason superseded
